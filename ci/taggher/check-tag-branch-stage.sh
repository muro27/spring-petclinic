#!/bin/bash

BRANCH_NAME="release-"$(echo $CI_COMMIT_REF_NAME | grep -o -E "[0-9]+\.[0-9]+\.[0-9]+")
if [[ $(git log -- origin/$BRANCH_NAME --pretty=oneline | grep $CI_COMMIT_SHA) ]]; then
  echo "CHECK OK: stage tag from branch release"
  exit 0
else
  echo "CHECK FAILED: tagged from wrong repository"
  exit -1
fi
