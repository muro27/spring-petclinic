#!/bin/bash
if [[ $(git tag --contains $CI_COMMIT_SHA) ]]; then
  echo "CHECK OK: wb tag from branch develop"
  exit 0
else
  echo "CHECK FAILED: tagged from wrong repository"
  exit -1
fi
